# Java Spring Boot Spring Data JPA
A helper project that illustrates the following Spring Data JPA relationships:
- One-to-One
- One-to-Many
- Many-to-Many

The goal is to become familar with Spring Data JPA by creating unit tests.  Placing break points and running unit tests in debug mode is a great way to learn Spring Data JPA.

# ERD
![](./docs/bookstore-erd.png)

- The [Bookstore](./src/main/java/com/example/demo/Model/Bookstore.java) has a many-to-many relationship with the [Book](./src/main/java/com/example/demo/Model/Book.java)
- The [Bookstore](./src/main/java/com/example/demo/Model/Bookstore.java) has a uni-directional one-to-many relationhip with the [Employee](./src/main/java/com/example/demo/Model/Employee.java)
- The [Employee](./src/main/java/com/example/demo/Model/Employee.java) has a uni-directional one-to-one relationship with the [Badge](./src/main/java/com/example/demo/Model/Badge.java)

Note: A bi-directional relationship requires adding a `mappedBy` attribute in the model class.

# Getting Started
- Clone this repo
- Create a schema in your local MySQL DB called `bookstore`
- Create `application.properties` file:
  - Create a file called `application.properties` based from [application.properties.template](./src/resources/application.properties.template)
  - Update the xxx in this file to the correct values for your local dev envionment
- Run the `PopulateDB()` unit test
- Observe the tables in your MySQL database:
```
select * from employee;
select * from bookstore;
select * from book;
select * from bookstore_book;
select * from badge;
```

# Derived Queries
Add these types of methods in your repository interface to query your model objects:
- [Query Methods](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods)
- [Baeldung](https://www.baeldung.com/spring-data-derived-queries)
- Quick Reference:
```
findBy<Your property>
findBy<Your property>Containing
findBy<Your property>StartingWith
findBy<Your property>EndingWith
findBy<Your property>Like
findBy<Your property>LessThan
findBy<Your property>LessThanEqual
findBy<Your property>In
findBy<Your property>Between
findBy<Your property>NotNull
findBy<Your property>Is
findBy<Your property>Equals
findBy<Your property>IsNot
findBy<Your property>IsNull
findBy<Your property>IsNotNull
findBy<Your property>{Or|And}<Your other property>
findBy<Your property>OrderByAsc<Your other Property>
findBy<Your property>OrderByDesc<Your other Property>
```

# Hibernate
- [JPA & Hibernate: Entity Lifecycle Model](https://youtu.be/Y7PpjerZkc0)
- Lifecycle states:
  - Transient
  - Managed 
  - Detached
  - Removed

# Links
- [Spring Data JPA Tutorial | Full In-depth Course](https://youtu.be/XszpXoII9Sg)
- [Generated Values Strategy Types](https://ngdeveloper.com/generationtype-identity-vs-generationtype-sequence-vs-generationtype-auto/)
- [Hybernate Life Cycle](https://www.javatpoint.com/hibernate-lifecycle)
- [Spring Web](https://docs.spring.io/spring-boot/docs/2.7.3/reference/htmlsingle/#web)
- [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.7.3/reference/htmlsingle/#data.sql.jpa-and-spring-data)
- [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
- [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
- [Building REST services with Spring](https://spring.io/guides/tutorials/rest/)
- [Accessing data with MySQL](https://spring.io/guides/gs/accessing-data-mysql/)
- [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
- [Creating your first Spring Boot API](./spring-init.md)

# Notes
- Collections are lazy loaded by default. 
- `findbyId()` vs `getById()`
  - Find fetches the data from the DB
  - Get is more of a lazy load approach
- Make sure you understand the hibernate life cycle
- `@Embedded/@Embedable` model objects occupy the same row as their parent object
