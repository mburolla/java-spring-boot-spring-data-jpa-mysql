package com.example.demo;

import org.junit.jupiter.api.Test;
import com.example.demo.Model.Book;
import org.springframework.util.Assert;
import com.example.demo.Repository.BookRepository;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;

@SpringBootTest
class BookTests {

    @Autowired
    private BookRepository bookRepository;

    @Test
    void createRecallDelete() {
        var title = "Relentless";
        var isbn = "9781118517710";

        // Create...
        var book = new Book(isbn, title);
        book = bookRepository.save(book); // This book contains the newly inserted id.
        Assert.notNull(book, "Book has not been stored in the database.");
        Assert.isTrue(book.getId() > 0, "Book id is not correct.");
        Assert.isTrue(book.getTitle().equals(title), "Book title is not correct.");
        Assert.isTrue(book.getIsbn().equals(isbn), "Book isbn is not correct.");

        // Recall...
        var recallBook = bookRepository.findById(book.getId()).get();
        System.out.println(recallBook);
        Assert.notNull(book, "Book has not been recalled from the database.");
        Assert.isTrue(recallBook.getId().equals(book.getId()), "Book id is not correct.");
        Assert.isTrue(recallBook.getTitle().equals(book.getTitle()), "Book title is not correct.");
        Assert.isTrue(recallBook.getIsbn().equals(book.getIsbn()), "Book title is not correct.");

        // Delete...
        bookRepository.deleteById(recallBook.getId());
        var emptyBook = bookRepository.findById(recallBook.getId());
        Assert.isTrue(emptyBook.isEmpty(), "Book is still in database.");
    }

    @Test
    void queryBook() {
        var books = bookRepository.findByTitleContaining("Title 4");
        Assert.isTrue(books.size() == 1, "Missing Book.  Did you forget to populate the database with the unit test?");
    }

}
