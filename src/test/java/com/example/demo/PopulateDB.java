package com.example.demo;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.jupiter.api.Test;
import com.example.demo.Model.Badge;
import com.example.demo.Model.Book;
import com.example.demo.Model.Employee;
import com.example.demo.Model.Bookstore;
import com.example.demo.Repository.BadgeRepository;
import com.example.demo.Repository.BookRepository;
import com.example.demo.Repository.EmployeeRepository;
import com.example.demo.Repository.BookstoreRepository;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;

@SpringBootTest
public class PopulateDB {
    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private BadgeRepository badgeRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private BookstoreRepository bookstoreRepository;

    @Test
    void populateDB() throws ParseException {
        // Create Employees and their Badges.
        SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
        var badge1 = new Badge(true, format.parse("01-01-2022" ), null);
        var employee1 = new Employee("Alice", "Jones");
        employee1.addBadge(badge1);

        var badge2 = new Badge(true, format.parse("01-02-2022" ), null);
        var employee2 = new Employee("Bob", "Marley");
        employee2.addBadge(badge2);

        var badge3 = new Badge(true, format.parse("01-03-2022" ), null);
        var employee3 = new Employee("Charlie", "Sanders");
        employee3.addBadge(badge3);

        var badge4 = new Badge(true, format.parse("01-04-2022" ), null);
        var employee4 = new Employee("Dave", "Mathews");
        employee4.addBadge(badge4);

        var badge5 = new Badge(true, format.parse("01-05-2022" ), null);
        var employee5 = new Employee("Egar", "Winter");
        employee5.addBadge(badge5);

        var badge6 = new Badge(true, format.parse("01-06-2022" ), null);
        var employee6 = new Employee("Frank", "Zappa");
        employee6.addBadge(badge6);

        // Create books.
        var book1 = new Book("1111111111", "Book Title 1");
        var book2 = new Book("2222222222", "Book Title 2");
        var book3 = new Book("3333333333", "Book Title 3");
        bookRepository.save(book1);
        bookRepository.save(book2);
        bookRepository.save(book3);

        // Create bookstores.
        var bookstore1 = new Bookstore("Barnes and Noble", "123 Green Street", "Webster", "NY", "14580");
        var bookstore2 = new Bookstore("Books are Us", "777 Good Luck Street", "Ontario", "NY", "14519");
        var bookstore3 = new Bookstore("Book World", "5150 Brown Street", "Richmond", "VA", "23237");

        // Give the bookstores some books.
        bookstore1.addBook(book1);
        bookstore2.addBook(book1);
        bookstore2.addBook(book2);
        bookstore3.addBook(book1);
        bookstore3.addBook(book2);
        bookstore3.addBook(book3);

        // Assign employees to the bookstores.
        bookstore1.addEmployee(employee1);
        bookstore2.addEmployee(employee2);
        bookstore2.addEmployee(employee3);
        bookstore3.addEmployee(employee4);
        bookstore3.addEmployee(employee5);
        bookstore3.addEmployee(employee6);

        // Link the books to the bookstores in the database.
        bookstoreRepository.save(bookstore1);
        bookstoreRepository.save(bookstore2);
        bookstoreRepository.save(bookstore3);
    }
}
