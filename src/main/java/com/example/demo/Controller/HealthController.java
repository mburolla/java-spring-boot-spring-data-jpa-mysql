package com.example.demo.Controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {

    @GetMapping("api/v1/health")
    public ResponseEntity<String> getHealther() {
        return new ResponseEntity<String>("OK", HttpStatus.OK);
    }
}

