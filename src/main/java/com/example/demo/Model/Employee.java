package com.example.demo.Model;

import javax.persistence.*;

@Entity
public class Employee {

    //
    // Data members
    //

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;
    private String firstName;
    private String lastName;

    // bookstore_id column is defined in Bookstore.

    @OneToOne(targetEntity = Badge.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="badge_id", referencedColumnName = "Id")
    // badge_id is the name of the column in the employee table.
    private Badge badge;

    //
    // Constructors
    //

    public Employee() {
    }

    public Employee(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    //
    // Methods
    //

    public void addBadge(Badge badge) {
        this.badge = badge;
    }

    //
    // Accessors
    //

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    //
    // Overrides
    //

    @Override
    public String toString() {
        return "Employee{" +
                "Id=" + Id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
