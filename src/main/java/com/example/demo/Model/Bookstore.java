package com.example.demo.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Bookstore {

    //
    // Data members
    //

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;
    private String name;
    private String street;
    private String city;
    private String state;
    private String zip;

    @OneToMany(targetEntity = Employee.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="bookstore_id", referencedColumnName = "Id")
    // bookstore_id is name of new column in employee (foreign) table.
    // referencedColumnName is the name of the column/property in THIS class.
    private List<Employee> employeeList = new ArrayList<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "bookstore_book",
            joinColumns = @JoinColumn(name = "bookstore_id"),
            inverseJoinColumns = @JoinColumn(name = "book_id") // Foreign table.
    )
    private Set<Book> bookList = new HashSet<>(); // Use a set to avoid multiple bag error.

    //
    // Constructors
    //

    public Bookstore() {
    }

    public Bookstore(String name, String street, String city, String state, String zip) {
        this.name = name;
        this.street = street;
        this.city = city;
        this.state = state;
        this.zip = zip;
    }

    //
    // Methods
    //

    public void addEmployee(Employee employee) {
        employeeList.add(employee);
    }

    public void addBook(Book book) {
        bookList.add(book);
    }

    //
    // Accessors
    //

    public Set<Book> getBookList() {
        return bookList;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    //
    // Overrides
    //

    @Override
    public String toString() {
        return "Bookstore{" +
                "Id=" + Id +
                ", name='" + name + '\'' +
                ", street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", zip='" + zip + '\'' +
                '}';
    }
}
